/*
 * @Description:
 * @Autor: youhui
 * @Date: 2023-04-20 15:28:22
 * @LastEditors: youhui
 * @LastEditTime: 2023-04-24 16:27:57
 */
// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint'
  },
  env: {
    browser: true,
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/essential',
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    'standard'
  ],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  // add your custom rules here
  rules: {
    // allow async-await
    // 'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    "singleQuote": true, // 使用单引号
    "printWidth": 115,
    "proseWrap": "always",
    "semi": 0, // 不加分号
    "no-unused-vars":"off",
    "trailingComma": "none", // 结尾处不加逗号
    "htmlWhitespaceSensitivity": "ignore" // 忽略'>'下落问题
  }
}
