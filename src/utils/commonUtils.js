/*
 * @Description:
 * @Autor: youhui
 * @Date: 2023-04-27 11:13:11
 * @LastEditors: youhui
 * @LastEditTime: 2023-04-27 15:53:12
 */
// 深拷贝
export function deepCopy (obj) {
  const result = Array.isArray(obj) ? [] : {}
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (typeof obj[key] === 'object' && obj[key] !== null) {
        if (obj[key] instanceof RegExp) {
          result[key] = obj[key]
        } else {
          result[key] = deepCopy(obj[key]) // 递归复制
        }
      } else {
        result[key] = obj[key]
      }
    }
  }
  return result
}
