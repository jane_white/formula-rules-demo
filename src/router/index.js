/*
 * @Description:
 * @Autor: youhui
 * @Date: 2023-04-14 15:55:10
 * @LastEditors: youhui
 * @LastEditTime: 2023-04-21 15:58:59
 */
import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '../home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage
    }
  ]
})
