/*
 * @Description:
 * @Autor: youhui
 * @Date: 2023-05-08 14:41:57
 * @LastEditors: youhui
 * @LastEditTime: 2023-05-08 14:43:04
 */
export default {
  methods: {
    // -------------------------------------------  数学函数  -----------------------------------------------
    // 求和
    sum () {
      try {
        if (!arguments[0] || arguments.length < 2) throw new Error()
        let result = 0
        for (let i = 0; i < arguments.length; i++) {
          result = result + arguments[i]
        }
        return result
      } catch (error) {
        this.$message({
          message: '校验失败，请检查SUM公式规则！',
          type: 'warning'
        });
      }
    },
    // 相减
    sub () {
      try {
        if (!arguments[0] || arguments.length < 2) throw new Error()
        let result = arguments[0]
        for (let i = 1; i < arguments.length; i++) {
          result = result - arguments[i]
        }
        return result
      } catch (error) {
        this.$message({
          message: '校验失败，请检查SUB公式规则！',
          type: 'warning'
        });
      }
    },
    // if判断函数
    judgeIf (judgeCode, trueCode, falseCode) {
      try {
        if (arguments.length !== 3) throw new Error()
        return judgeCode ? trueCode : falseCode
      } catch (error) {
        this.$message({
          message: '校验失败，请检查IF公式规则！',
          type: 'warning'
        });
      }
    },
    // 向下取整
    int1 () {
      try {
        if (!arguments[0]) throw new Error()
        return parseInt(arguments[0])
      } catch (error) {
        this.$message({
          message: '校验失败，请检查向下取整函数INT1公式规则！',
          type: 'warning'
        });
      }
    },
    // 向上取整
    int2 () {
      try {
        if (!arguments[0]) throw new Error()
        return parseInt(arguments[0]) + 1
      } catch (error) {
        this.$message({
          message: '校验失败，请检查向上取整函数INT2公式规则！',
          type: 'warning'
        });
      }
    },
    // 四舍五入取整
    int3 () {
      try {
        if (!arguments[0]) throw new Error()
        return Math.round(arguments[0])
      } catch (error) {
        this.$message({
          message: '校验失败，请检查四舍五入取整函数INT3公式规则！',
          type: 'warning'
        });
      }
    }
  }
}
