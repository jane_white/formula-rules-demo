/*
 * @Description:
 * @Autor: youhui
 * @Date: 2023-12-05 10:37:17
 * @LastEditors: youhui
 * @LastEditTime: 2023-12-05 11:21:06
 */
export const originFormFieldsList = [
  {
    label: '表单1',
    value: 'form1',
    children: [
      {
        label: '表单字段1',
        uuid: 'uuid-11',
        value: 'field1'
      },
      {
        label: '表单字段2',
        uuid: 'uuid-12',
        value: 'field2'
      },
      {
        label: '表单字段3',
        uuid: 'uuid-13',
        value: 'field3'
      },
      {
        label: '表单字段4',
        uuid: 'uuid-14',
        value: 'field4'
      },
      {
        label: '表单字段5',
        uuid: 'uuid-15',
        value: 'field5'
      },
      {
        label: '表单字段6',
        uuid: 'uuid-16',
        value: 'field6'
      }
    ]
  },
  {
    label: '表单2',
    value: 'form2',
    children: [
      {
        label: '表单字段1',
        uuid: 'uuid-21',
        value: 'field1'
      },
      {
        label: '表单字段2',
        uuid: 'uuid-22',
        value: 'field2'
      },
      {
        label: '表单字段3',
        uuid: 'uuid-23',
        value: 'field3'
      },
      {
        label: '表单字段4',
        uuid: 'uuid-24',
        value: 'field4'
      },
      {
        label: '表单字段5',
        uuid: 'uuid-25',
        value: 'field5'
      },
      {
        label: '表单字段6',
        uuid: 'uuid-26',
        value: 'field6'
      }
    ]
  },
  {
    label: '表单3',
    value: 'form3',
    children: [
      {
        label: '表单字段1',
        uuid: 'uuid-31',
        value: 'field1'
      },
      {
        label: '表单字段2',
        uuid: 'uuid-32',
        value: 'field2'
      },
      {
        label: '表单字段3',
        uuid: 'uuid-33',
        value: 'field3'
      },
      {
        label: '表单字段4',
        uuid: 'uuid-34',
        value: 'field4'
      },
      {
        label: '表单字段5',
        uuid: 'uuid-35',
        value: 'field5'
      },
      {
        label: '表单字段6',
        uuid: 'uuid-36',
        value: 'field6'
      }
    ]
  }
]
