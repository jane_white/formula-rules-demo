/*
 * @Description:
 * @Autor: youhui
 * @Date: 2023-04-24 15:58:06
 * @LastEditors: youhui
 * @LastEditTime: 2023-04-26 16:40:26
 */
import {nanoid} from 'nanoid'
export const singleSymbolBtnList = [
  {
    label: '表单字段',
    code: 'formFields',
    templateName: 'formFields',
    type: 'customTemplate',
    id: nanoid()
  },
  {
    label: '+',
    code: 'plusSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: '-',
    code: 'minusSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: '*',
    code: 'multSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: '/',
    code: 'divisionSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: '>',
    code: 'greaterThanSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: '>=',
    code: 'greaterThanOrEqualSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: '<',
    code: 'lessThanSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: '<=',
    code: 'lessThanOrEqualSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: '=',
    code: 'equalSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: '(',
    code: 'leftEnlargementSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: ')',
    code: 'rightEnlargementSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: ',',
    code: 'commaSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: '"',
    code: 'doubleQuotesSign',
    type: 'bottomSingleSign',
    id: nanoid()
  },
  {
    label: '静态值',
    code: 'staticValue',
    templateName: 'staticValue',
    type: 'customTemplate',
    bindValue: '',
    id: nanoid()
  },
  {
    label: 'TRUE',
    code: 'isTrue',
    type: 'customTemplate',
    templateName: 'booleanValue',
    color: '#f5f7fa',
    id: nanoid()
  },
  {
    label: 'FALSE',
    code: 'isFalse',
    type: 'customTemplate',
    templateName: 'booleanValue',
    color: '#f5f7fa',
    id: nanoid()
  }
]

export const formFieldsOptions = [
  {
    value: 'testForm1',
    label: '测试表单1',
    children: [
      {
        value: 'formFields1',
        uuid: '',
        label: '表单字段1'
      },
      {
        value: 'formFields2',
        uuid: '',
        label: '表单字段2'
      },
      {
        value: 'formFields3',
        uuid: '',
        label: '表单字段3'
      },
      {
        value: 'formFields4',
        uuid: '',
        label: '表单字段4'
      }
    ]
  },
  {
    value: 'testForm2',
    label: '测试表单2',
    children: [
      {
        value: 'formFields1',
        uuid: '',
        label: '表单字段1'
      },
      {
        value: 'formFields2',
        uuid: '',
        label: '表单字段2'
      },
      {
        value: 'formFields3',
        uuid: '',
        label: '表单字段3'
      },
      {
        value: 'formFields4',
        uuid: '',
        label: '表单字段4'
      }
    ]
  }
]
