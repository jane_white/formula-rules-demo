/*
 * @Description:
 * @Autor: youhui
 * @Date: 2023-04-27 10:37:40
 * @LastEditors: youhui
 * @LastEditTime: 2023-06-07 08:48:11
 */
import { nanoid } from 'nanoid'
// 数学函数数组
export const originMathFuncsArr = [
  {
    component: 'add',
    id: nanoid(),
    label: 'SUM',
    description: 'SUM(字段1, 字段2, …)：仅支持对数字、金额类型字段的求和',
    shotDescribe: '多字段求和',
    itemComponents: [
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', cursorInsertIndex: 4, type: 'customTemplate', id: nanoid() },
      { keyWord: 'SUM', label: 'SUM', code: 'plusSign', type: 'customTemplate', id: nanoid(), templateName: 'booleanValue', color: '#f5f7fa' },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { keyWord: '(', label: '(', code: 'leftEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: ',', label: ',', code: 'commaSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { keyWord: ')', label: ')', code: 'rightEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() }
    ]
  },
  {
    component: 'subtraction',
    id: nanoid(),
    label: 'SUB',
    description: 'SUB(字段1(被减数), 字段2, …)：仅支持对数字、金额类型字段的相减计算',
    shotDescribe: '多字段相减',
    itemComponents: [
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', cursorInsertIndex: 4, type: 'customTemplate', id: nanoid() },
      { keyWord: 'SUB', label: 'SUB', code: 'subSign', type: 'customTemplate', id: nanoid(), templateName: 'booleanValue', color: '#f5f7fa' },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { keyWord: '(', label: '(', code: 'leftEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: ',', label: ',', code: 'commaSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { keyWord: ')', label: ')', code: 'rightEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() }
    ]
  }
]
// 逻辑函数数组
export const originLogicalFuncsArr = [
  {
    component: 'logical_if',
    id: nanoid(),
    label: 'IF',
    description: 'IF(条件,A,B) ：如果满足条件，默认返回TRUE，否则返回FALSE，A/B可替换为其他输入值或事件，支持嵌套多层函数。',
    shotDescribe: '根据条件执行对应事件',
    itemComponents: [
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', cursorInsertIndex: 4, type: 'customTemplate', id: nanoid() },
      { keyWord: 'IF', label: 'IF', code: 'ifSign', type: 'customTemplate', id: nanoid(), templateName: 'booleanValue', color: '#f5f7fa' },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { keyWord: '(', label: '(', code: 'leftEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: ',', label: ',', code: 'commaSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { label: 'TRUE', code: 'isTrue', type: 'customTemplate', templateName: 'booleanValue', color: '#f5f7fa', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { keyWord: ',', label: ',', code: 'commaSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { label: 'FALSE', code: 'isFalse', type: 'customTemplate', templateName: 'booleanValue', color: '#f5f7fa', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { keyWord: ')', label: ')', code: 'rightEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() }
    ]
  },
  {
    component: 'logical_int',
    id: nanoid(),
    label: 'INT1',
    description: 'INT(A) ：对参数A进行去尾取整',
    shotDescribe: '向下取整',
    intType: 'down',
    itemComponents: [
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', cursorInsertIndex: 4, type: 'customTemplate', id: nanoid() },
      { keyWord: 'INT', label: 'INT1', intType: 'down', code: 'downIntSign', type: 'customTemplate', id: nanoid(), templateName: 'booleanValue', color: '#f5f7fa' },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { keyWord: '(', label: '(', code: 'leftEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: ')', label: ')', code: 'rightEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() }
    ]
  },
  {
    component: 'logical_int',
    id: nanoid(),
    label: 'INT2',
    description: 'INT(A) ：对参数A进行进一取整',
    shotDescribe: '向上取整',
    intType: 'up',
    itemComponents: [
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', cursorInsertIndex: 4, type: 'customTemplate', id: nanoid() },
      { keyWord: 'INT', label: 'INT2', intType: 'up', code: 'downIntSign', type: 'customTemplate', id: nanoid(), templateName: 'booleanValue', color: '#f5f7fa' },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { keyWord: '(', label: '(', code: 'leftEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: ')', label: ')', code: 'rightEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() }
    ]
  },
  {
    component: 'logical_int',
    id: nanoid(),
    label: 'INT3',
    description: 'INT(A) ：对参数A进行四舍五入取整',
    shotDescribe: '四舍五入取整',
    intType: 'round',
    itemComponents: [
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', cursorInsertIndex: 4, type: 'customTemplate', id: nanoid() },
      { keyWord: 'INT', label: 'INT3', intType: 'round', code: 'downIntSign', type: 'customTemplate', id: nanoid(), templateName: 'booleanValue', color: '#f5f7fa' },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() },
      { keyWord: '(', label: '(', code: 'leftEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: ')', label: ')', code: 'rightEnlargementSign', type: 'bottomSingleSign', id: nanoid() },
      { keyWord: '', label: '', code: 'emptyDiv', templateName: 'emptyDiv', type: 'customTemplate', id: nanoid() }
    ]
  }
]
